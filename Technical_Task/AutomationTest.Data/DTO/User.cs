﻿namespace AutomationTest.Data.DTO
{
    public class User
    {
        public string FirstName { get; set; } = "Dmitro";
        public string LastName { get; set; } = "Pavliuk";
        public string Email { get; set; } = "paulyukdmitro@gmail.com";
        public string Office { get; set; } = "London";
        public string Gender { get; set; } = "Male";
    }
}
