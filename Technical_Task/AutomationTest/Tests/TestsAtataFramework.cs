﻿using AutomationTest.Data.DTO;
using AutomationTest.PageObject.AtataFramework;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace AutomationTest.Tests
{
    [TestFixture]
    internal class TestsAtataFramework
    {
        private readonly User _user;
        public TestsAtataFramework()
        {
            _user = new User();
        }
        [Test]
        public void MainTest()
        {
            var driver = new FirefoxDriver { Url = "https://atata-framework.github.io/atata-sample-app/#!/signin" };
            var loginPage = new LoginPage(driver);
            var userPage = loginPage.LoginAs("admin@mail.com", "abc123");
            var newUserPage = userPage.NavigateToNewUserPage();
            newUserPage.CreateUser(_user);
            userPage.VerifyСreatingUser(_user.Email);

            var viewPage = userPage.NavigateToView(_user);
            viewPage.VerifyUser(_user);
            driver.Close();
        }

        [Test]
        public void LoginTest()
        {
            var driver = new FirefoxDriver { Url = "https://atata-framework.github.io/atata-sample-app/#!/signin" };
            var loginPage = new LoginPage(driver);
            loginPage.LoginAs("admin@mail.com", "abc123");
            driver.Close();
        }
        [Test]
        public void CreateUserTest()
        {
            var driver = new FirefoxDriver { Url = "https://atata-framework.github.io/atata-sample-app/#!/signin" };
            var loginPage = new LoginPage(driver);
            var userPage = loginPage.LoginAs("admin@mail.com", "abc123");
            var newUserPage = userPage.NavigateToNewUserPage();
            newUserPage.CreateUser(_user);
            driver.Close();
        }

        [Test]
        public void VerifyCreatingUser()
        {
            var driver = new FirefoxDriver { Url = "https://atata-framework.github.io/atata-sample-app/#!/signin" };
            var loginPage = new LoginPage(driver);
            var userPage = loginPage.LoginAs("admin@mail.com", "abc123");
            var newUserPage = userPage.NavigateToNewUserPage();
            newUserPage.CreateUser(_user);
            userPage.VerifyСreatingUser(_user.Email);
            driver.Close();
        }

        [Test]
        public void VerifyUserDatails()
        {
            var driver = new FirefoxDriver { Url = "https://atata-framework.github.io/atata-sample-app/#!/signin" };
            var loginPage = new LoginPage(driver);
            var userPage = loginPage.LoginAs("admin@mail.com", "abc123");
            var newUserPage = userPage.NavigateToNewUserPage();
            newUserPage.CreateUser(_user);
            userPage.VerifyСreatingUser(_user.Email);

            var viewPage = userPage.NavigateToView(_user);
            viewPage.VerifyUser(_user);
            driver.Close();
        }
    }
}
