﻿using System;
using OpenQA.Selenium;

namespace AutomationTest.PageObject.AtataFramework
{
    internal class LoginPage
    {
        private readonly IWebDriver _driver;


        private readonly By _emailLocation = By.Id("email");
        private readonly By _passwordLocation = By.Id("password");
        private readonly By _loginButtonLocation = By.XPath("/html/body/div/div/div[2]/input");

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
            if (!_driver.Title.Contains("Sign In"))
            {
                throw  new Exception("This is not the login page");
            }
        }



        protected LoginPage TypeEmail(string email)
        {
            _driver.FindElement(_emailLocation).SendKeys(email);
            return this;
        }

        protected LoginPage TypePassword(string password)
        {
            _driver.FindElement(_passwordLocation).SendKeys(password);
            return this;
        }

        protected UsersPage SubmitLogin()
        {
            _driver.FindElement(_loginButtonLocation).Submit();
            return new UsersPage(_driver);
        }

        public UsersPage LoginAs(string email, string password)
        {
            TypeEmail(email);
            TypePassword(password);
            return SubmitLogin();
        }



    }
}
