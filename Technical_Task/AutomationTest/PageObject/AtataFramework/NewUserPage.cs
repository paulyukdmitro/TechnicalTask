﻿using System;
using System.IO;
using AutomationTest.Data.DTO;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutomationTest.PageObject.AtataFramework
{
    internal class NewUserPage
    {
        private readonly IWebDriver _driver;
        private readonly By _firstNameLocation = By.Id("first-name");
        private readonly By _lastNameLocation = By.Id("last-name");
        private readonly By _emailLocation = By.Id("email");
        private readonly By _officeDropdownLocation = By.Id("office");
        private readonly By _maleGenderLocation = By.XPath("//*[@id=\"general\"]/div[5]/label[2]/input");
        private readonly By _femaleGenderLocation = By.XPath("//*[@id=\"general\"]/div[5]/label[3]/input");
        private readonly By _createUserButtonLocation = By.XPath("/html/body/div[1]/div[2]/div/div/div[3]/button[1]");

        public NewUserPage(IWebDriver driver)
        {
            _driver = driver;
            if (!_driver.Title.Contains("Users"))
            {
                throw new Exception("This is not the users page");
            }
        }
        protected NewUserPage TypeFirstName(string firstName)
        {
            _driver.FindElement(_firstNameLocation).SendKeys(firstName);
            return this;
        }
        protected NewUserPage TypeLastName(string lastName)
        {
            _driver.FindElement(_lastNameLocation).SendKeys(lastName);
            return this;
        }
        protected NewUserPage TypeEmail(string email)
        {
            _driver.FindElement(_emailLocation).SendKeys(email);
            return this;
        }
        protected NewUserPage TypeOffice(string office)
        {
            var selectOffice = _driver.FindElement(_officeDropdownLocation);
            var dropdown = new SelectElement(selectOffice);

            dropdown.SelectByValue(office);
            return this;
        }
        protected NewUserPage TypeGender(string gender)
        {
            IWebElement genderWebElement;

            switch (gender)
            {
                case "Male":
                {
                    genderWebElement = _driver.FindElement(_maleGenderLocation);
                    break;
                }
                case "Female":
                {
                    genderWebElement = _driver.FindElement(_femaleGenderLocation);
                    break;
                }
                default: throw new InvalidDataException();
            }
            genderWebElement.Click();

            return this;
        }
        protected UsersPage SubmitCreate()
        {
            _driver.FindElement(_createUserButtonLocation).Click();
            return new UsersPage(_driver);
        }

        public UsersPage CreateUser(User user)
        {
            TypeFirstName(user.FirstName);
            TypeLastName(user.LastName);
            TypeGender(user.Gender);
            TypeEmail(user.Email);
            TypeOffice(user.Office);

            return SubmitCreate();
        }
    }
}
