﻿using System;
using System.Linq;
using AutomationTest.Data.DTO;
using OpenQA.Selenium;

namespace AutomationTest.PageObject.AtataFramework
{
    internal class UsersPage
    {
        private readonly IWebDriver _driver;

        public UsersPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public UsersPage VerifyСreatingUser(string userEmail)
        {
            var userChack = _driver.FindElement(By.XPath("/html/body/div/div[1]/div[2]/table/tbody")).GetAttribute("innerHTML")
                .Contains(userEmail);

            if (!userChack)
            {
                throw new Exception();
            }

            return this;
        }

        public NewUserPage NavigateToNewUserPage()
        {
            var newButton = _driver.FindElement(By.XPath("/html/body/div/div[1]/button"));
            newButton.Submit();
            return new NewUserPage(_driver);
        }

        public ViewPage NavigateToView(User user)
        {
            var userChack = _driver.FindElements(By.XPath("/html/body/div/div[1]/div[2]/table/tbody/tr"))
                .FirstOrDefault(p => p.Text.Contains(user.Email));

            if (userChack == null)
            {
                throw new Exception();
            }

            userChack.FindElement(By.XPath(".//div/a")).Click();

            return new ViewPage(_driver);
        }


    }
}
