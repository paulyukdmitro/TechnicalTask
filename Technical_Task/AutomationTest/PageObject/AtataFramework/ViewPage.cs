﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationTest.Data.DTO;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomationTest.PageObject.AtataFramework
{
    internal class ViewPage
    {
        private readonly IWebDriver _driver;

        public ViewPage(IWebDriver driver)
        {
            _driver = driver;
        }

        protected ViewPage VerifyUserEmail(string email)
        {
            var verifyEmail = _driver.FindElements(By.ClassName("col-sm-6"))
                    .FirstOrDefault(p => p.FindElement(By.TagName("dt")).Text.Contains("Email"))
                    .FindElement(By.TagName("dd"))
                    .Text
                    .Contains(email)
                ;


            if (!verifyEmail)
            {
                throw new Exception($"Email has not been verified");
            }

            return this;
        }
        protected ViewPage VerifyUserOffice(string office)
        {
            var verifyOffice = _driver.FindElements(By.ClassName("col-sm-6"))
                    .FirstOrDefault(p => p.FindElement(By.TagName("dt")).Text.Contains("Office"))
                    .FindElement(By.TagName("dd"))
                    .Text
                    .Contains(office)
                ;


            if (!verifyOffice)
            {
                throw new Exception($"Office has not been verified");
            }

            return this;
        }
        protected ViewPage VerifyUserGender(string gender)
        {
            var verifyOffice = _driver.FindElements(By.ClassName("col-sm-6"))
                    .FirstOrDefault(p => p.FindElement(By.TagName("dt")).Text.Contains("Gender"))
                    .FindElement(By.TagName("dd"))
                    .Text
                    .Contains(gender)
                ;


            if (!verifyOffice)
            {
                throw new Exception($"Gender has not been verified");
            }

            return this;
        }
        protected ViewPage VerifyUserFirstName(string firstName)
        {
            var verifyFirstName = _driver.FindElement(By.XPath("/html/body/div/div/div[1]/h1"))
                .Text
                .Contains(firstName);

            if (!verifyFirstName)
            {
                throw new Exception($"First name has not been verified");
            }

            return this;
        }
        protected ViewPage VerifyUserLastName(string lastName)
        {
            var verifyLastName = _driver.FindElement(By.XPath("/html/body/div/div/div[1]/h1"))
                .Text
                .Contains(lastName);

            if (!verifyLastName)
            {
                throw new Exception($"Last name has not been verified");
            }

            return this;
        }
      
        public ViewPage VerifyUser(User user)
        {
            VerifyUserEmail(user.Email);
            VerifyUserOffice(user.Office);
            VerifyUserFirstName(user.FirstName);
            VerifyUserGender(user.Gender);
            VerifyUserLastName(user.LastName);
            
            return this;
        }
    }
}
